#mycompilerV3

AU début du confinement je ne pouvais pas travailler l'assembleur chez moi car je n'avais pas FLex ni Bison sur mon PC Windows. J'ai fini par demander à la DOSI de l'installer sur le serveur pedago de l'université, ce qui a été fait. Néanmoins j'avais beaucoup de retard dans les TP, et avec les autres matières en parallèle, dont le travail à fournir est aussi conséquent, je n'ai pas pu le rattraper.
Ainsi afin d'avoir une base solide j'ai téléchargé la dernière version du CERIcompiler de M.Jourlin.
J'ai rajouté la boucle "FOR" avec "TO" et "DOWNTO" et aussi avec "STEP" (le pas d'incrémentation de la variable du "FOR"). De plus, pour des raisons pratiques d'affichage, j'ai rajouté le mot-clé "LNDISPLAY" qui permet d'aller à la ligne après l'affichage (comme un "println" en JAVA).


Voici les grammaires et quelques exemples (On suppose que les variables sont déjà définies au début du programme):

        #FOR

// ForStatement := "For" AssignementStatement ":=" SimpleExpression ("TO"|"DOWNTO") SimpleExpression {"STEP" SimpleExpression} "DO" Statement

La comparaison de la variable avec la valeur dans le TO/DOWNTO se fait à la fin de la boucle, ainsi la boucle est parcourue au moins une fois.

Ex1 :
...
a:=0;
FOR i:=0 TO 5 DO a := a+1;      // a vaudra 6
...


Ex2 :
...
a:=0;
FOR i:=5 DOWNTO 0 DO a := a+1;      // a vaudra 6
...

Ex3 :
...
a:=0;
FOR i:=0 TO 80 STEP 10 DO a := a+1;     // a vaudra 9
...

Le compilateur ne prend pas en charge les nombres négatifs et il est de la responsabilité du programmeur d'utiliser la boucle FOR correctement. Voici quelques exemples qui ne donneront pas le résultat escompté :

Ex4 :
...
a:=0;
FOR i:=15 TO 5 DO a := a+1;      // a vaudra 1 (i=15, a=1, comparaison i<=5 : faux donc stop)
...

Ex5 :
...
a:=0;
FOR i:=0 TO 5 STEP 100 DO a := a+1;      // a vaudra 2 (i=0, a=1, comparaison i<=5 : ok donc continue | i=100, a=2, comparaison i<=100 : faux donc stop)
...


        #LNDISPLAY
"LNDISPLAY" évite de faire des "DISPLAY '\n'" dans le programme pour aller à la ligne.

// DisplayStatement := "DISPLAY" Expression || "LNDISPLAY" Expression

Ex comparaison DISPLAY et LNDISPLAY :
...
DISPLAY 1;
DISPLAY 2.
...

--> Affichera :
12

...
LNDISPLAY 1;
LNDISPLAY 2.
...

--> Affichera :
1
2

